; Load register 1 with 7
ldi r1,7

; Load register 2 with 17
ldi r2,17

; add contents of register 2 to register 1 and store in register 0
ADD R0,R1,R2

; subtract contents of register 0 and store in register 2 so 2 will store 0
SUB R2,R0,R0

; store 5 in register 2
MOV R2,#5
