#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int encode_ldi(char *line, unsigned char *opcode);
int same_operands(char *line);
int define_add(char *line, unsigned char *opcode);
int define_sub(char *line, unsigned char *opcode);
int define_mov(char *line, unsigned char *opcode);
int _11rd();
int _10rd();



/*
 * LDI: Load Immediate, Rd <- K
 * opcode: 1110 KKKK dddd KKKK
 * operands: 16 <= d <= 31, 0 <= K <= 255

 * ADD: Add without carry, Rd <- Rd + Rr
 * opcode: 0000 11rd dddd rrrr
 * operands: 0 <= d <= 31, 0 <= r <= 31
 *
 * SUB: Subtract without carry, Rd <- Rd - Rr
 * opcode: 0001 10rd dddd rrrr
 * operands: 0 <= d <=31, 0 <= r <=31
 *
 * MOV: Copy Register Rd <- Rr
 * opcode: 0010 11rd dddd rrrr
 * operands: 0 <= d <= 31, 0 <= r <= 31
 */


//* is not a pointer but data being read from stream and not stored
 
static int encode_ldi(char *line, unsigned char *opcode)
{
	unsigned int d, K;

	if (sscanf(line, "ldi r%u,%u", &d, &K) == 2) {

		/* Sanity check operands */
		if (d < 16 || d > 31)
			return -1;
		if (K > 255)
			return -1;

		unsigned int code = 0xE000;

		code |= ((K << 4) & 0x0F00);
		code |= ((d << 4) & 0x00F0);
		code |= ((K)      & 0x000F);

		opcode[0] = code;
		opcode[1] = (code >> 8);

		return 0;
	}

	return -1;

}

static int same_operands(char *line){

	 unsigned int d, r;

        if (sscanf(line, "ldi r%u,%u", &d, &r) == 2) {

                /* Sanity check operands */
                if (d < 0 || d > 31)
                        return -1;
                if (r < 0 || r > 31)
                        return -1;

		unsigned int code;

		 code |= ((d << 4) & 0x00F0);
                 code |= ((r)      & 0x000F);


		opcode[0] = code;
                opcode[1] = (code >> 8);


                return 0;
        }

        return -1;

}	

static int _11rd(){

	int same_operands(char *line);

	code |= ((11rd << 4) & 0x0F00);
}

static int _10rd(){

	int same_operands(char *line);

	 code |= ((10rd << 4) & 0x0F00);

}

static int define_add(char *line, unsigned char *opcode){

	static int _11rd();

        code = 0x0000 << 12;  

	return 0;
}

static int define_sub(char *line, unsigned char *opcode){

	static int _10rd();

	code = 0x1000 << 12;

	return 0;
}

static int define_mov(char *line, unsigned char *opcode){
        
	static int _11rd();

	code = 0x2000 << 12;

	return 0;
}

int main()
{
	/* removed int argc, char *argv[] and switch statement?? and errno??*/

	/* how does opcode work? three instead of two*/
	unsigned char opcode[2];

	if (!encode_ldi("ldi r24,10", opcode))
		printf("%c%c", opcode[0], opcode[1]);

	if (!encode_ldi("ldi r25,255", opcode))
		printf("%c%c", opcode[0], opcode[1]);

	if (!encode_ldi("ldi r26,255", opcode))
		printf("%c%c", opcode[0], opcode[1]);


	if (!define_add("ADD r0, r1, r2", opcode))
                printf("%c%c", opcode[0], opcode[1]);

        if (!define_add("ADD r0, r1, r3", opcode))
                printf("%c%c", opcode[0], opcode[1]);


	if (!define_sub("SUB r2, r0, r0", opcode))
                printf("%c%c", opcode[0], opcode[1]);

        if (!define_sub("SUB r2, r0, r1", opcode))
                printf("%c%c", opcode[0], opcode[1]);


	if (!define_mov("MOV r2, #5", opcode))
                printf("%c%c", opcode[0], opcode[1]);

        if (!define_mov("MOV r2, #7", opcode))
                printf("%c%c", opcode[0], opcode[1]);


	return 0;
}
