TARGET:=gcas
SOURCES:=gcas.c
CC:=avr-gcc
mmcu=atmega328p
CFLAGS+=-ggdb -Wall -I -mmcu=$(MCU)
OBJECT_FILES=gcas.o
TEST:=test


all: $(TARGET)

$(TEST): $(TARGET)
	@avr-gcc -nostartfiles -mmcu=atmega328p test.s -o test.o
	@avr-objcopy -O binary -j .text test.o test.bin
	@echo
	@echo -- $(TARGET) output
	@./$(TARGET) | hexdump -C
	@echo
	@echo -- $(TEST) output
	@hexdump -C test.bin

cppcheck:
	cppcheck --enable=all -q $(TARGET)

compare:
	diff -u a.out > gcas.s [$$? -eq 1]
	$(warning differences detected)

clean:
	$(RM) $(TARGET) $(TARGET).o $(TARGET).bin
	$(RM) $(TEST).o $(TEST).bin

.PHONY: all clean cppcheck compare $(TEST)


