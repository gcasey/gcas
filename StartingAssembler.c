#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main (int argc, char *argv[]){


    if( argc > 3 ) {
          printf("Too many arguments supplied.\n");
       }

    
    char s1[12];
    FILE *input_file  = fopen(argv[1], "r"); // read only
    if (input_file == NULL)
            {
              fprintf(stderr, "Error! Could not open input file %s,%s\n", argv[1], strerror(errno));
              //perror("fopen_input%s", strerror(errno)); 
              exit(-1); 
            } 

    FILE *output_file = fopen(argv[2], "w+"); // write only plus creates 
    if (output_file == NULL) 
            {   
              fprintf(stderr, "Error! Could not open output file %s , %s\n",argv[2], strerror(errno));
	      //perror("fopen_output%s", strerror(errno)); 
              exit(-1); 
            } 

      while (fscanf(input_file, "%s", &s1) ==1){

if (s1[0] != '\n'| ';'){


       if (!strcmp(s1,"ADD"))
           {
            fputs("ADD r0, r1, r2", output_file);
	    
           }
       else if (!strcmp(s1,"SUB"))
           {
            fputs("SUB r2, r2, r0", output_file);

           }
       else if (!strcmp(s1,"MOV"))
           {
            fputs("MOV r2, #5", output_file);

           }
}
      }


      fclose(input_file);
      fclose(output_file);
     
   
}

